<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * Localized language
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/* Add any custom values between this line and the "stop editing" line. */



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */

ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);

if ( ! defined( 'WP_DEBUG' ) ) {
	define( 'WP_DEBUG', false );
}

define( 'WP_ENVIRONMENT_TYPE', 'local' );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define('AUTH_KEY',         'lnEbsHzKsBZLe4A7DcBY/32KEfZzTKie/VUR5JdDB2HPz5bAfwqwGmuFqCpW77mmiNwei/tma4zRoUjlrtzmEg==');
define('SECURE_AUTH_KEY',  'xVMlRAwnkG+sSXzxhrOgKQmwt+1RsjlnavXV3ssXTm0s9yCUHEp9lgvh+0fcj9Su++iB3+hYdfmPc2xnMGI1LA==');
define('LOGGED_IN_KEY',    '26Y3ZY/ic+7g3lXvAu4QOmMfAMTuZTjdlLgcONbJq/A66FPchfAusmDVEaG9FSV4ANcKDiXSM9EQ3B9va0EWnw==');
define('NONCE_KEY',        'cE5TvsIrG4lG0fLyYqgEnkZwXRsFE2XmYDk7wHBcpe2sSf/9N5gVDmpykSjI63a2R1QY4qKW6I2azg0dzlE8mg==');
define('AUTH_SALT',        'FmQgcklK08sjw4ovG+YOUuO+C4foPTm5JK2gfCSAeFZqbsOSqY10hnJJULwNgSGgr3k4rZKM3D25NzpxWyI6ZA==');
define('SECURE_AUTH_SALT', 'gjPVsvzMKzFeFnVSLH22/EsjU9FR6eyYw3mmwyQh+f7sxdjuIHx/Sqrhw/+lYTGHWT8ZTS5EVC7rCdcD9ezjuA==');
define('LOGGED_IN_SALT',   'Wq1koVw9z72qb9JlDKHhYUwPJTcHhDNIdU9VSRngq1RxyNqMsmdjv7Wni2FJ3oNCgLdmxDYhE6CMDmAZxinIRg==');
define('NONCE_SALT',       'ws/7ma5Vr/SOc340fi/o4+2HoL6ykw5OzF1sFdN4GT9Jug7eXsIThJjHl2xRwYStH64vZ2FCJUYF1C6HAObZVA==');