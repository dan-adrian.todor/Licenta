=== Shipping Cost on Product Page Calculator for WooCommerce ===

Contributors: octolize,grola,sebastianpisula
Tags: product page shipping calculator, shipping calculator, shipping estimate, shipping cost, woocommerce product page, woocommerce product page shipping calculator
Requires at least: 5.7
Tested up to: 6.2
Stable tag: 1.3.6
Requires PHP: 7.4
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Let your customers calculate and see the shipping cost on product pages based on the entered shipping destination and cart contents.

== Description ==

= The best free plugin to display shipping cost on product page =

Use this free Shipping Cost on Product Page Calculator for WooCommerce plugin to display the shipping cost calculator on product pages in your WooCommerce store. Allow your customers to check the available shipping methods for the shipping address they enter and let them know about the shipping cost of the products they are about to order. **Provide them with all the important shipping information right away on the product page, even before reaching the cart or checkout**.

**UX studies have proven that placing a shipping cost calculator on a product page results in customers making a purchase decision much easier.** Don't lose your customers, improve the shipping experience and increase the sales in your store!

= Features =

* Displaying the shipping cost calculator on product pages
* Hiding the shipping cost calculator on the virtual products' pages

= PRO Features =

* Displaying the available shipping methods automatically based on the saved shipping address or WooCommerce settings
* Selecting the specific products to calculate the shipping cost for
* Hiding the shipping cost calculator on selected product pages
* Advanced calculator placement management
* Placing the calculator anywhere you need it with customizable shortcodes

[Upgrade to PRO now →](https://octol.io/scpp-repo-upgrade)

= Actively developed and supported =

The  Shipping Cost on Product Page Calculator for WooCommerce plugin is constantly being developed by Octolize. Our plugins are used by over **250.000 WooCommerce stores worldwide**. Over the years we proved to have become not only the authors of stable and high-quality plugins, but also as a team providing excellent technical support. Join the community of our satisfied plugins' users. Bet on quality and let our plugins do the rest.

= Docs =

View the dedicated [Shipping Cost on Product Page Calculator Documentation](https://octol.io/scpp-repo-docs)

= Interested in plugin translations? =

We are actively looking for contributors to translate this and [other Octolize plugins](https://profiles.wordpress.org/octolize/#content-plugins). Each supported language tremendously help store owners to conveniently manage shipping operations.

Your translations contribute to the WordPress community at large. Moreover, we're glad to offer you discounts for our PRO plugins and establish long-term collaboration. If you have any translation related questions, please email us at [translations@octolize.com](mailto:translations@octolize.com).

Head over here and help us to translate this plugin:
[https://translate.wordpress.org/projects/wp-plugins/octolize-shipping-cost-on-product-page/](https://translate.wordpress.org/projects/wp-plugins/octolize-shipping-cost-on-product-page/)

== Installation	 ==

This plugin can be easily installed like any other WordPress integration by following the steps below:

1. Download and unzip the latest zip file release.
2. Upload the entire plugin directory to your **/wp-content/plugins/** path.
3. Activate the plugin using the **Plugins** menu in WordPress sidebar menu.

Optionally you can also try to upload the plugin zip file using **Plugins &rarr; Add New &rarr; Upload Plugin** option from the WordPress sidebar menu. Then go directly to point 3.

== Screenshots ==

1. Shipping Cost on Product Page general settings
2. Shipping cost calculator displayed on product page

== Changelog ==

= 1.3.6 - 2023-06-12 =
* Added support for WooCommerce 7.8

= 1.3.5 - 2023-05-10 =
* Added support for WooCommerce 7.7

= 1.3.4 - 2023-03-27 =
* Added support for WordPress 6.2
* Added support for WooCommerce 7.6

= 1.3.3 - 2023-03-09 =
* Added support for WooCommerce 7.5

= 1.3.2 - 2023-01-25 =
* Fixed Cart Value calculation (Flexible Shipping)

= 1.3.1 - 2023-01-10 =
* Added support for WooCommerce 7.3

= 1.3.0 - 2022-11-28 =
* Added support for WooCommerce 7.2
* Added WooCommerce High-Performance Order Storage (HPOS) compatibility declaration

= 1.2.0 - 2022-11-02 =
* Added possibility to not display the shipping cost calculator on the virtual products' product pages

= 1.1.3 - 2022-10-19 =
* Added support for WooCommerce 7.1

= 1.1.2 - 2022-10-19 =
* Added support for WordPress 6.1

= 1.1.1 - 2022-10-03 =
* Added support for WooCommerce 7.0

= 1.1.0 - 2022-09-20 =
* Added state field

= 1.0.1 - 2022-08-09 =
* Changed calculator position

= 1.0.0 - 2022-08-02 =
* First release!
