��    "      ,  /   <      �     �     
     (     <  !   K  "   m  *   �  .   �  .   �          *  '   I  	   q     {     �     �     �     �     �     �     �          (     7     T     r     �     �     �  
   �     �  8   �     &  F  =     �     �     �     �  (   �  2      4   3  1   h  3   �     �  3   �  3   	     H	     X	     m	     �	     �	     �	     �	     �	     �	     
     
     $
     '
     D
     _
     b
     u
     �
     �
  8   �
     �
                !                         
                                              	                                 "                                       Powered by %s.   Spicy Recipe | Developed By  Appearance Settings Blossom Themes Change color and body background. Change the font size of your site. Choose the home page layout for your site. Choose the layout of the header for your site. Choose the layout of the slider for your site. Continue Reading Demo Link: %1$sClick here.%2$s Documentation Link: %1$sClick here.%2$s Font Size Header Layout Home Page Layout Information Links Primary Color Primary Font Primary Font: on or offon Primary color of the theme. Primary font of the site. Secondary Color Secondary Font Secondary Font: on or offon Secondary color of the theme. Secondary font of the site. Site Title Font: on or offon Slider Layout Spicy Recipe Typography https://blossomthemes.com/ https://blossomthemes.com/wordpress-themes/spicy-recipe/ https://wordpress.org/ PO-Revision-Date: 2023-06-07 13:14:07+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n == 0 || n % 100 >= 2 && n % 100 <= 19) ? 1 : 2);
X-Generator: GlotPress/4.0.0-alpha.4
Language: ro
Project-Id-Version: Themes - Spicy Recipe
  Propulsată de %s.   Spicy Recipe | Dezvoltată de  Setări aspect Blossom Themes Schimbă culoarea și fundalul corpului. Schimbă dimensiunea fontului pentru site-ul tău. Alege aranjamentul pentru prima pagină a site-ului. Alege aranjamentul antetului pentru site-ul tău. Alege aranjamentul caruselului pentru site-ul tău. Continuă lectura Legătură la demonstrație: %1$sdă clic aici.%2$s Legătură la documentație: %1$sdă clic aici.%2$s Dimensiune font Aranjament în antet Aranjament prima pagină Legături pentru informații Culoare principală Font principal on Culoarea principală a temei. Font principal pentru site. Culoare secundară Font secundar on Culoarea secundară a temei. Font secundar pentru site. on Aranjament carusel Spicy Recipe Tipografice https://blossomthemes.com/ https://blossomthemes.com/wordpress-themes/spicy-recipe/ https://wordpress.org/ 